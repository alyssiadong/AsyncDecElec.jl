"""
Launch calculations
"""

function launch_simulation(ademo::AbstractDecElecProblem)
    sim = Simulation() 	# Simulation environment

    Mailboxes = instantiate_mailboxes(sim, ademo)
    Trigs = instantiate_agent_simulation(sim, ademo, Mailboxes)

    conv = @process checkConvergence(sim, ademo)

    res, comp_time, bytes, gctime, memallocs = @timed begin
        try
            SimJulia.run(sim, conv)
        catch ex 
            # (now(sim)==0.0) && println(now(sim))
            rethrow(ex)
        end
    end

    return DecElec.finalize(ademo)
end


### P2P problem
function instantiate_agent_simulation(env::Environment, ademo::P2PProblem, mailboxes)
	prosumers = ademo.content.agents
	
    Trigs = Array{Process, 1}(undef, length(prosumers)) # Array that saves trigger processes for every agent
	for (i, agent) in prosumers
		Trigs[i] = @process asynchronous_agent(env, agent, Trigs, mailboxes) 
	end

	return Trigs
end

function instantiate_mailboxes(env::Environment, ademo::P2PProblem)
    Mailboxes = Dict{Int, Any}()
	for (i,agent) in ademo.content.agents   # Create mailboxes for each prosumer
        box = Store{Message}(env)
        meta = Dict{Symbol,Any}()
        meta[:id_agent] = i
        meta[:trigger_level] = 1                                            # trigger level always set to 1 for p2p problem

        current = Dict{Symbol,Any}()
        current[:local_iters]  = Dict{Symbol,Any}(:trade => Dict())
        current[:iters]        = Dict{Symbol,Any}(:trade => Dict())
        current[:load]         = Dict{Symbol,Any}(:trade => Dict())
        for (idx, neighbors) in async_param(agent, :trade)
            n_arc, from, to = idx 
            new_idx = n_arc, to, from
            current[:local_iters][:trade][new_idx] = 0                             # initiating local iteration for shared value
            current[:iters][:trade][new_idx] = Dict(nei => 0 for nei in neighbors) # initiating iteration from last received message (that matches with local iter)
            current[:load][:trade][new_idx]  = Dict(nei => 0 for nei in neighbors) # initiating number of messages in mailbox (that matches with local iter)
        end

        meta[:ntrig] = Dict{Symbol,Any}(:trade => ceil(async_param(agent, :delta_trade)*length(current[:load][:trade])))

        Mailboxes[i] = Mailbox(box, meta, current)
    end
    return Mailboxes
end