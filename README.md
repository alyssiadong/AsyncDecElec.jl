# AsyncDecElec

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://alyssiadong.gitlab.io/AsyncDecElec.jl/dev)
[![Build Status](https://gitlab.com/alyssiadong/AsyncDecElec.jl/badges/master/pipeline.svg)](https://gitlab.com/alyssiadong/AsyncDecElec.jl/pipelines)
[![Coverage](https://gitlab.com/alyssiadong/AsyncDecElec.jl/badges/master/coverage.svg)](https://gitlab.com/alyssiadong/AsyncDecElec.jl/commits/master)
