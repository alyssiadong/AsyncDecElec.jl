module AsyncDecElec

import DecElec
import DecElec: build_group, ref_add_global_core!, parse_csv, ref
import DecElec: AbstractDecElecProblem, DEAbstractContent, AbstractDecElecContainer, AbstractDecElecAgent, GroupOfAgents
import DecElec: P2PProblem, P2PAgent
import DecElec: ref, sol

import CSV
import DataFrames
import DataFrames: DataFrame

import JuMP
import Ipopt
import OSQP

import ResumableFunctions, SimJulia
import ResumableFunctions: @resumable
import SimJulia: Environment, Process, @process, Simulation, Store, timeout, @yield, now, put

import AxisArrays
import LinearAlgebra
import LinearAlgebra: norm, Diagonal
import SparseArrays: I, sparse
import LightGraphs: SimpleGraph, add_edge!, neighbors, nv, ne

import PlotlyJS
import PlotlyJS: plot, scatter, GenericTrace, Layout

import Memento

# Create our module level logger (this will get precompiled)
const _LOGGER = Memento.getlogger(@__MODULE__)

# Register the module level logger at runtime so that folks can access the logger via `getlogger(PowerModels)`
# NOTE: If this line is not included then the precompiled `PowerModels._LOGGER` won't be registered at runtime.
__init__() = Memento.register(_LOGGER)

"Suppresses information and warning messages output by PowerModels, for fine grained control use the Memento package"
function silence()
    Memento.info(_LOGGER, "Suppressing information and warning messages for the rest of this session.  Use the Memento package for more fine-grained control of logging.")
    # Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
    # Memento.setlevel!(Memento.getlogger(PowerModels), "error")
    Memento.setlevel!(Memento.getlogger(DecElec), "error")
    Memento.setlevel!(Memento.getlogger(AsyncDecElec), "error")
end

"alows the user to set the logging level without the need to add Memento"
function logger_config!(level)
    Memento.config!(Memento.getlogger("AsyncDecElec"), level)
end

include("core/type.jl")
include("core/base.jl")

include("prob/p2p.jl")

include("model/comm.jl")
include("model/comp.jl")

include("sim/mes.jl")
include("sim/conv.jl")
include("sim/agent.jl")
include("sim/launch.jl")

end
