"""
Checking for convergence
"""

@resumable function checkConvergence(env::Environment, ademo::AbstractDecElecProblem)
	T_final = 300.0 # à modifier
	δt = 1.0 # à modifier
	Eprim = 1e-3 # à modifier
	while now(env) < T_final
	    @yield timeout(env, δt)
	    println("test $(now(env))")
	    if ( 0.0 < ademo.sol["primal_res"][0] ≤ Eprim)
	        break
	    end
	end
end