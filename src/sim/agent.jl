"""
Simulate a single agent
"""

@resumable function asynchronous_agent(env::Environment, agent::AbstractDecElecAgent, trigger_array, mailbox_array) 

	# first iteration
	iter_computation(agent)
	@process iter_send(env, agent, trigger_array, mailbox_array)
	
    trig = @process iter_receive(env, agent) 	# Wait for iteration trigger: number of messages received ntrig
    try
        @yield trig
    catch ex
    	println("out!!")
    	nothing
    end

    @process retrieve_messages(env, agent, mailbox_array[ref(agent, :prosumer, "id_agent")])

	# # 
	# while true
	# 	update_problem(agent)
	# 	update_monitor(agent)
	# 	iter_computation(agent)
	# 	iter_communication(agent)
	# end

	# # return finalize(agent)
end


@resumable function iter_send(env::Environment, agent::AbstractDecElecAgent, trigger_array, mailbox_array)
	# sends all new messages to neighbors, groups data by neighbor
	for (nei, dic) in async_param(agent, :neighbors_arcs)
		meta = Dict{Symbol,Any}()
		data = Dict{Symbol,Any}()
		meta[:send] = ref(agent, :id_agent)
		for (type, indexes) in dic 
			for idx in indexes
				if idx ∈ agent.ext[:async][Symbol(type, "_temp")]
					meta[:receive] = nei
					if !haskey(meta, type)
						meta[type] = [idx]
						data[type] = Dict(idx => Dict{Symbol,Any}(:data => sol(agent, String(type), idx), :iteration => sol(agent, string(type, "_iter"), idx)))
					else
						push!(meta[type], idx)
						data[type][idx] = Dict{Symbol,Any}(:data => sol(agent, String(type), idx), :iteration => sol(agent, string(type, "_iter"), idx))
					end
				end
			end
		end

		trigger = trigger_array[nei]
		mailbox = mailbox_array[nei]
		message = Message(meta, data)
		# delay = sample_delay(agent, message)
		delay = 10.0 + randn()
		@process send_message(env, message, delay, trigger, mailbox)
	end
end


@resumable function iter_receive(env::Environment, agent::AbstractDecElecAgent)
	# waits for a maximum of T_max for messages to arrive
	T_max = async_param(agent, :T_max)
	@yield timeout(env, T_max)
end

@resumable function retrieve_messages(env::Environment, agent::AbstractDecElecAgent, mailbox)
	# reset_mailbox_count(mailbox)
	# # Read messages 
	# Nreceived = length(mailbox.mailbox.items)
	# J1 = Array{Int,1}()         # J1 lists all agents id 
	#                             # from which we received messages
	# sendBack = Array{Message,1}()
	# for m∈1:Nreceived           # Retrieving messages one at a time
	#     try 
	#         mes = @yield get(mailbox.mailbox)
	#         # Filter incoming messages { j∈ωi/ matching iteration }
	#         if (mes.kij == kij[mes.i]) || (iterMatching == "nomatching")    
	#             j = mes.i
	#             !(j ∈ ωi) && throw(UnconsistentPartner)
	#             push!(J1, j)
	#             tji[j] = mes.tij
	#             kji[j] = mes.kij
	#         elseif (mes.kij > kij[mes.i])
	#             push!(sendBack, mes)
	#         else
	#             println("kij = ", kij[mes.i],". Trash ", mes.kij,  "th message from ", mes.i, " to ", mes.j)
	#         end
	#     catch exc
	#         println("Agent", i,  " could not retrieve message ", m,".")
	#         println("   Queue[i]:")
	#         display(Queues[i].items)
	#         rethrow(exc)
	#     end
	# end
end