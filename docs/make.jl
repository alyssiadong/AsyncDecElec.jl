using AsyncDecElec
using Documenter

DocMeta.setdocmeta!(AsyncDecElec, :DocTestSetup, :(using AsyncDecElec); recursive=true)

makedocs(;
    modules=[AsyncDecElec],
    authors="Alyssia Dong <alyssia.dong@ens-rennes.fr> and contributors",
    repo="https://gitlab.com/alyssiadong/AsyncDecElec.jl/blob/{commit}{path}#{line}",
    sitename="AsyncDecElec.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://alyssiadong.gitlab.io/AsyncDecElec.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
