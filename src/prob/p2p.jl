function run_async_p2p(file; kwargs...)
	data = parse_csv(file)
	ademo = instantiate_model(data, P2PProblem, GroupOfAgents(), 
				build_group, ref_add_global_core!; kwargs)
	launch_simulation(ademo)

	return ademo
end

"""
Adding asynchronous related data to problem
"""
function ext_add_async_agent!(agent::P2PAgent, global_ext::Dict)
    id_agent = ref(agent, :prosumer, "id_agent")
    agent.ext[:async] = Dict{Symbol,Any}() # async params

    # agent ref
    agent.ext[:async][:trade] = trade_dic = deepcopy(global_ext[:trade][id_agent])
    agent.ext[:async][:trade_temp] = [idx for (idx,v) in trade_dic] # current indexes taken into account during current iteration 
    agent.ext[:async][:T_max] = global_ext[:T_max]
    agent.ext[:async][:delta_trade] 	= δtrade = global_ext[:delta_trade]
    agent.ext[:async][:trigger_trade] 	= Int(ceil(δtrade*length(trade_dic)))
    agent.ext[:async][:neighbors] = unique(vcat([v for (idx,v) in trade_dic]...))
    agent.ext[:async][:neighbors_arcs] = Dict(v => Dict(:trade => [(n_arc,i,j) for ((n_arc,i,j), val) in trade_dic if j==v]) 
    										for v in agent.ext[:async][:neighbors])
end

function sol_add_async!(agent::AbstractDecElecAgent)
   	 agent.sol["trade_iter"] = dic = Dict()
   	 for id in agent.var[:trade]
   	 	dic[id] = 0
   	 end
   	 agent.sol["agent_iter"] = 0
end

"""
Modifying iteration functions
"""

function iter_computation(agent::P2PAgent)
	m = agent.content.model

	results = OSQP.solve!(m)            # Solve !
	!(results.info.status == :Solved) && println(results.info.status)
	T_sol = results.x                   # x = Arg min(problem)

	i = ref(agent, :prosumer, "id_agent")
	for (k,idx) in enumerate(async_param(agent, :trade_temp))
		agent.sol["trade_prev"][idx] = sol(agent, "trade", idx)
		agent.sol["trade"][idx] = T_sol[k]
	end
	agent.sol["power"][i] = sum(T_sol)
end

