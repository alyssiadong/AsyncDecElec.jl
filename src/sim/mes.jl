"""
Message structure and functions
"""

struct Message
    meta::Dict{Symbol,Any}
    data::Dict{Symbol,Any}

    # Blank constructor
    function Message()
        new(Dict{Symbol,Any}(), Dict{Symbol,Any}())
    end
    # Regular constructors
    function Message(send::Int, receive::Int, meta::Dict{Symbol,<:Any}, data::Dict{Symbol,<:Any})
        meta[:send] = send
        meta[:receive] = receive
        new(meta, data)
    end
    function Message(meta::Dict{Symbol,<:Any}, data::Dict{Symbol,<:Any})
        @assert (haskey(meta, :send) && haskey(meta, :receive)) 
        new(meta, data)
    end
end


@resumable function send_message(env::Environment, mes::Message, delay::Number, trigger, mailbox)
    @yield timeout(env, delay)
    @yield put(mailbox.mailbox, mes)
    increment_mailbox_count(mailbox, mes)

    trig = check_mailbox(mailbox) 
    if trig
        @yield SimJulia.interrupt(trigger)
    end
end



"""
Mailbox structure and functions
"""
struct Mailbox
    mailbox::Store{Message}
    meta::Dict{Symbol,Any}
    current::Dict{Symbol,Any}
end

iter(mailbox::Mailbox) = mailbox.current[:iters]
iter(mailbox::Mailbox, type::Symbol) = mailbox.current[:iters][type]
iter(mailbox::Mailbox, type::Symbol, idx) = mailbox.current[:iters][type][idx]

local_iter(mailbox::Mailbox) = mailbox.current[:local_iters]
local_iter(mailbox::Mailbox, type::Symbol) = mailbox.current[:local_iters][type]
local_iter(mailbox::Mailbox, type::Symbol, idx) = mailbox.current[:local_iters][type][idx]

load(mailbox::Mailbox) = mailbox.current[:load]
load(mailbox::Mailbox, type::Symbol) = mailbox.current[:load][type]
load(mailbox::Mailbox, type::Symbol, idx) = mailbox.current[:load][type][idx]

trigger_level(mailbox) = mailbox.meta[:trigger_level]

function check_mailbox(mailbox::Mailbox)
    # checks if enough messages have arrived to trigger next iteration
    if trigger_level(mailbox) == 0
        # considers every received messages that matches the local iteration
        Memento.error(_LOGGER, "This is yet to be implemented")
    elseif trigger_level(mailbox) == 1
        # considers every received messages that matches the local iteration by type
        trig = Dict{Symbol,Bool}()
        for (type, dic) in load(mailbox)
            trig[type] = (sum( sum( load for (nei, load) in dic_nei) for (idx,dic_nei) in dic) == mailbox.meta[:ntrig][type])
            # println("$(sum( sum( load for (nei, load) in dic_nei) for (idx,dic_nei) in dic)) ≥ $(mailbox.meta[:ntrig][type])")
        end

        final_trig = all(values(trig))
    elseif trigger_level(mailbox) == 2
        # considers every activated "idx" 
        Memento.error(_LOGGER, "This is yet to be implemented")
    else
        Memento.error(_LOGGER, "This trigger level does not correspond to anything.")
    end

    return final_trig
end

function increment_mailbox_count(mailbox::Mailbox, mes::Message)
    for (type, dic) in mes.data
        for (idx, data) in dic
            iteration = data[:iteration]
            mailbox_iteration = mailbox.current[:local_iters][type][idx]
            if iteration == mailbox_iteration
                mailbox.current[:load][type][idx][mes.meta[:send]] += 1
            end
        end
    end
end
function reset_mailbox_count(mailbox::Mailbox)
    for (type, dic) in mailbox.current[:load]
        for (idx, loads) in dic
            for (k,load) in loads
                loads[k] = 0
            end
        end
    end
end