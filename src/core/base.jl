function instantiate_model(data::Dict{String,<:Any}, model_type::Type, 
    content::DEAbstractContent, build_method, ref_add_core!; 
    ref_extensions=[], iter_max=1000, kwargs...)
    # NOTE, this model constructor will build the ref dict using the latest info from the data

    start_time = time()
    ademo = DecElec.instantiate_model(data, model_type, content, build_method, 
            ref_add_core!; ref_extensions = ref_extensions, iter_max = iter_max, kwargs...)
    Memento.debug(_LOGGER, "initialize basic decentralized model time: $(time() - start_time)")

    ext_add_async!(ademo)
    sol_add_async!(ademo)

    return ademo
end

function ext_add_async!(demo::AbstractDecElecProblem)
    demo.ext[:async] = Dict{Symbol,Any}()

    # asynchronous parameters
    if haskey(demo.ref, :T_max)
        demo.ext[:async][:T_max] = demo.ref[:T_max]
    else 
        Memento.warn(_LOGGER, "T_max not communicated, set to 1000.0")
        demo.ext[:async][:T_max] = 1000.0
    end

    if haskey(demo.ref, :delta)
        demo.ext[:async][:delta] = demo.ref[:delta]
    else
        Memento.warn(_LOGGER, "δ not communicated, set to 0.1")
        demo.ext[:async][:delta] = 0.1
    end

    if haskey(demo.ref, :delta_trade)
        demo.ext[:async][:delta_trade] = demo.ref[:delta_trade]
    else
        Memento.warn(_LOGGER, "δtrade not communicated, set to 0.1")
        demo.ext[:async][:delta_trade] = 0.1
    end

    # group ref
    if haskey(demo.ref, :prosumer_arcs)
        demo.ext[:async][:trade] = trade_dic = Dict{Int,Any}()
        for (k,v) in demo.ref[:prosumer_arcs]
            trade_dic[k] = prosumer_dic = Dict()
            for (n_arc, i, j) in v
                prosumer_dic[(n_arc,i,j)] = [j]
            end
        end
    end

    # agent ref specific to each problem
    for (n,agent) in demo.content.agents
        ext_add_async_agent!(agent, demo.ext[:async])
    end
end

async_param(demo::AbstractDecElecContainer) = demo.ext[:async]
async_param(demo::AbstractDecElecContainer, name::Symbol) = demo.ext[:async][name]
async_param(demo::AbstractDecElecContainer, name::Symbol, idx) = demo.ext[:async][name][idx]


function sol_add_async!(demo::AbstractDecElecProblem)
    demo.sol["iteration"] = Dict{Int, Any}(k=>0 for (k,v) in demo.content.agents)
    for (k,agent) in demo.content.agents
        sol_add_async!(agent)
    end
end
